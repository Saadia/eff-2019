<?php

use App\Http\Controllers\AssociationController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\StageController;
use App\Http\Controllers\VolontaireController;
use App\Models\Association;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::middleware("authenticated")->group(function(){
    Route::resource("volontaires",VolontaireController::class)->except(["create","store"]);    
    Route::get('/associations/show/{id}',[AssociationController::class,"show"])->name("associations.show");
    
    Route::get('/logout',[AuthController::class,"logout"])->name("auth.logout");

    Route::get('/listeStage',[StageController::class,"index"])->name("stages.index");
    Route::get('/stage/show/{stage}',[StageController::class,"show"])->name("stages.show");


});
Route::middleware("non_authenticated")->group(function(){
    Route::get('/login',[AuthController::class,"login_create"])->name("auth.login_create");
    Route::post('/login',[AuthController::class,"login_store"])->name("auth.login_store");
    Route::resource("volontaires",VolontaireController::class)->only(["create","store"]);
    // Route::get("/volontaire/create", [VolontaireController::class,"create"])->name("volontaires.create");
    // Route::post("/volontaire", [VolontaireController::class,"store"])->name("volontaires.store");
});

