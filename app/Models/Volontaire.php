<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Volontaire extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_vlt';
    protected $fillable = [ "nom_vlt", "prenom_vlt", "mail", "mot_passe", "ville_id"];
}
