<?php

namespace App\Models;

use App\Models\Stage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Association extends Model
{
    use HasFactory;
    protected $primaryKey="id_ass";
    protected $fillable = ["nom_ass", "raisonsocial", "telephone", "adresse", "ville_id"];

    public function stages(){
        return $this->hasMany(Stage::class,"association_id");
    }

}
