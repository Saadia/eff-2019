<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Demande_inscription extends Model
{
    use HasFactory;
    protected $fillable = ["id_inscription", "date_demande", "volontaire_id", "stage_id", "etat"];
}
