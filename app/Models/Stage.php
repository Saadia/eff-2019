<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    use HasFactory;
    protected $primaryKey="id_stage";
    protected $fillable = ["id_stage", "date_debut", "date_fin", "association_id"];


    public function association(){
        return $this->belongsTo(Association::class);
    }
}
