<?php

namespace App\Http\Controllers;

use App\Models\Ville;
use App\Models\Volontaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class VolontaireController extends Controller
{
    
   
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return view("volontaires.index");
    }

    /**
     * return the 'volontaires.create' view that shown a form for new registration
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $villes=Ville::all();
        return view('volontaires.create',compact("villes"));
    }

     /**
     * add a new user to data base
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse   
     *   */
    public function store(Request $request)
    {
        $validateData = $request->validate(
            [
                "nom_vlt" => "required",
                "prenom_vlt" => "required",
                "ville_id" => "required",
                "mail" => "required|email|unique:volontaires",
                "mot_passe" => "required|confirmed"
            ]
        );
        $validateData["mot_passe"]=Hash::make($request->mot_passe);
        Volontaire::create($validateData);
        return redirect()->route("auth.login_create");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
