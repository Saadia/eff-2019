<?php

namespace App\Http\Controllers;

use App\Models\Association;
use App\Models\Stage;
use Illuminate\Http\Request;

class StageController extends Controller
{
    public function index(){
        $associations=Association::all();
        return view('stages.listStage',compact("associations"));
    }

    public function show(Stage $stage){
        return view('stages.show',compact("stage"));

    }
}
