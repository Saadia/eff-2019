<?php

namespace App\Http\Controllers;

use App\Models\Ville;
use App\Models\Volontaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    /**
     * return the 'auth.login' view that shown a login form
     * @return \Illuminate\Contracts\View\View
     */

    public function login_create()
    {
        return view('auth.login');
    }
    public function login_store(Request $request)
    {
        $request->validate(
            [
                "mail" => "required|email",
                "mot_passe" => "required"
            ]
        );
        $attempts = session()->get('attempts', 0);
        $volontaire = Volontaire::where("mail", $request->mail)->first();

        if ($attempts == 3 && $volontaire) {
            $volontaire->active = false;
            $volontaire->save();
        }
        if (!$volontaire  || !Hash::check($request->mot_passe, $volontaire->mot_passe)) {
            session()->put('attempts', $attempts + 1);
            return redirect()->route('auth.login_create')->withErrors(["message" => "login ou mot de passe est incorrect"]);
        }
        session()->put('volontaire', $volontaire);
        session()->forget('attempts');
        $volontaire->active = true;
        $volontaire->save();
        return redirect()->route("volontaires.index");
    }

    public function logout(){
        session()->forget('volontaire');
        return redirect()->route('auth.login_create');
    }
}
