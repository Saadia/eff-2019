<?php

namespace App\Http\Controllers;

use App\Models\Association;
use Illuminate\Http\Request;

class AssociationController extends Controller
{
    //
    public function show($id){
        $association = Association::find($id);
        $stages=$association->stages;
        return response()->json(["association" => $association,"stages"=>$stages]);
    }
}
