<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('volontaires', function (Blueprint $table) {
            $table->id("id_vlt");
            $table->string("nom_vlt");
            $table->string("prenom_vlt");
            $table->string("mail")->unique();
            $table->string("mot_passe");
            $table->foreignId("ville_id")->constrained("villes","id_ville");
            $table->boolean("active")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('volontaires');
    }
};
