<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('demande_inscriptions', function (Blueprint $table) {
            $table->id("id_inscription");
            $table->date("date_demande");
            $table->foreignId("volontaire_id")->constrained("volontaires","id_vlt");
            $table->foreignId("stage_id")->constrained("stages","id_stage");
            $table->string("etat")->default('En attente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('demande_inscriotions');
    }
};
