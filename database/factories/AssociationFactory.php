<?php

namespace Database\Factories;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\ville;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Association>
 */
class AssociationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nom_ass' => $this->faker->company,
            'raisonsocial' => $this->faker->companySuffix,
            'adresse' => $this->faker->address,
            'telephone' => $this->faker->phoneNumber,
            'ville_id' => Ville::all()->random()->id_ville,
        ];
    }
}
