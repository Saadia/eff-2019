<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>@yield('title')</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @yield('style')

</head>

<body>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" id="navId" role="tablist">
        @if (session()->get('volontaire'))
        {{session()->get('volontaire')->id_vlt}}
        {{session()->get('volontaire')->nom_vlt}}
        {{session()->get('volontaire')->prenom_vlt}}
        <button > <a href="{{route("auth.logout")}}">Log out</a></button>
        @else
            <li class="nav-item">
                <a href={{ route('volontaires.create') }} class="nav-link active" data-bs-toggle="tab"
                    aria-current="page">S'inscrire</a>
            </li>
            <li class="nav-item">
                <a href={{ route('auth.login_create') }} class="nav-link active" data-bs-toggle="tab"
                    aria-current="page">Connextion</a>
            </li>
        @endif
    </ul>


    @yield('content')

</body>

</html>
