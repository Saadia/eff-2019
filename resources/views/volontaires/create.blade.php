@extends('layouts.app')
@section('title', "s'inscrire")
@section('content')
    <form action="{{ route('volontaires.store') }}" method="POST">
        @csrf
        <div>
            <label class="form-label" for="nom"> Nom </label>
            <input class="form-input" type="text" name="nom_vlt" id="nom" value={{old("nom_vlt")}}>
        </div>
        <div>
            <label class="form-label" for="prenom"> Prenom </label>
            <input class="form-input" type="text" name="prenom_vlt" id="prenom" value={{old("prenom_vlt")}} >
        </div>
        <div>
            <label for="ville">Ville</label>
            <select name="ville_id" id="ville">
                @foreach($villes as $ville)
                <option value={{$ville->id_ville}}>{{$ville->nom_ville}}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label class="form-label" for="mail"> Email </label>
            <input class="form-input" type="email" name="mail" id="mail" value={{old("mail")}}>
        </div>
        <div>
            <label class="form-label" for="mot_passe"> Mot De Pass </label>
            <input class="form-input" type="password" name="mot_passe" id="mot_passe" />
        </div>
        <div>
            <label class="form-label" for="mot_passe_c"> Confirmer Mot De Pass </label>
            <input class="form-input" type="password" name="mot_passe_confirmation" id="mot_passe_c" />
        </div>
        <input type="submit" value="S'inscrire">
    </form>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li> 
        @endforeach
    @endif
@endsection
