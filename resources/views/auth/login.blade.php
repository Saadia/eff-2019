@extends('layouts.app')
@section('title', 'login')
@section('content')
    <form action="{{route("auth.login_store")}}" method="post">
        @csrf
        <label for="mail">Login</label>
        <input type="text" name="mail" id="mail">
        <label for="mot_passe">Mot de pass</label>
        <input type="password" name="mot_passe" id="mot_passe">
        <input type="submit" value="Se connecter">
    </form>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @error('message') 
    @enderror
@endsection