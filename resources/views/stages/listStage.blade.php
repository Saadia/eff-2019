@extends('layouts.app')
@section('title', 'ListStage')
@section('style')
    <style>
        tr,
        td,
        th {
            border: 1px solid;
            border-collapse: collapse;
            padding: 1em;
        }
    </style>
@endsection
@section('content')
    <h2>Inscription a un stage</h2>
    <label for="association">Associations :</label>
    <select name="association" id="association">
        @foreach ($associations as $association)
            <option value={{ $association->id_ass }}>ASSOTIATION {{ $association->nom_ass }}</option>
        @endforeach
    </select>
    <h3>Liste des stages relatifs a l'association : <span id="nomAssociation">{{ $associations[0]->nom_ass }}</span></h3>
    <table>
        <thead>
            <tr>
                <th>Id stage</th>
                <th>Date debut</th>
                <th>Date fin</th>
                <th>Selectioner</th>
            </tr>
        </thead>
        <tbody id="tbody">
            {{-- @foreach ($associations[0]->stages as $stage)
                <tr>
                    <td class="idTd">{{ $stage->id_stage }}</td>
                    <td>{{ $stage->date_debut }}</td>
                    <td>{{ $stage->date_fin }}</td>
                    <td><button class="selectBtn" onclick="handleClick()">Selectioner</button></td>
                </tr>
            @endforeach --}}
        </tbody>
    </table>
    <div id="detaileStage">

    </div>


    <script type="module">
        function handleClick() {
                console.log(1)
            }
        $(document).ready(function() {
            
            $("#association").on("change", function(event) {
                let id = event.target.value;
                $("#tbody").text("")
                $.ajax({
                    type: "GET",
                    url: `/associations/show/${id}`,
                    success: function(response) {
                        $("#nomAssociation").text(response.association.nom_ass)
                        console.log(response.stages)
                        response.stages.map((stage) => {
                            console.log(stage.date_debut)
                            $("#tbody").append(`
                                <tr>
                                    <td classe="idTd"> ${ stage.id_stage }</td> 
                                    <td> ${ stage.date_debut} </td> 
                                    <td> ${ stage.date_fin} </td>
                                    <td> <button class="selectBtn" onclick="handleClick" > Selectioner </button></td >
                                </tr>`)
                        })
                    }
                })
            })

            // $(".selectBtn").on('click', function(e) {
            //     console.log($(this))
            //     let stage = $(this).closest("tr").find("td:first").text()
            //     $.ajax({
            //         type: "GET",
            //         url: `/stage/show/${stage}`,
            //         success: function(response) {
            //             $("#detaileStage").html(response)

            //         }
            //     })
            // });


        })
    </script>
@endsection
